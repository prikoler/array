//Создать массив элементов 'кол-во дней в месяцах' содержащих количество дней в соответствующем месяце
let numbers = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

//Создать массив элементов 'название месяцов' содержащий названия месяцев
let names = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"]

//Используя цикл for и массив 'кол-во дней в месяцах' выведите количество дней в каждом месяце (без имен месяцев)
for i in numbers {
    print(i)
}

//Используйте еще один массив с именами месяцев, чтобы вывести название месяца + количество дней
for i in 0..<numbers.count {
    print("\(names[i]) - \(numbers[i])")
}

//Сделайте тоже самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)
let tuple = [("январь", 31), ("февраль", 28), ("март", 31), ("апрель", 30), ("май", 31), ("июнь", 30), ("июль", 31), ("август", 31), ("сентябрь", 30), ("октябрь", 31), ("ноябрь", 30), ("декабрь", 31)]

for i in 1..<tuple.count {
    print("\(tuple[i].0) - \(tuple[i].1)")
}

//Сделайте тоже самое, только выводите дни в обратном порядке
var g = numbers.count
for i in 0..<numbers.count {
    g -= 1
    print("\(names[i]) - \(numbers[g])")
}

//Для произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года

let month = 5 // Май
let day = 10

var daysCount = 0

for i in 0..<(month - 1) {
    daysCount += numbers[i]
}
daysCount += day

print("Количество дней от начала года до \(day).\(month): \(daysCount)")
